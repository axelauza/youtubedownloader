/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadsSource;
import Controller.SyncPipe;
import Model.Pelicula;
import static View.MainFrame.SavePath;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
/**
 *
 * @author Axel
 */
public class HiloDescarga extends Thread{
    
    Pelicula pelicula;
    int listaAc;
    String size;
    
    public HiloDescarga(Pelicula pelicula,int listaAc,String size) {
        this.pelicula=pelicula;
        this.listaAc=listaAc;
        this.size=size;
    }
    public void run(){
        if (listaAc==1 && !pelicula.isList()){
            return;
        }
        File miDir = new File (".");
        String download_path="";
        try {
            System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
            download_path=miDir.getCanonicalPath();
        } catch (IOException ex) {
            System.out.println("Error Descarga");
        }
        //String url="https://www.youtube.com/watch?v=m_1IsltDJcA";
            String[] command =
        {
            "cmd",
        };
        download_path=SavePath;
	Process p;
        try {
                p = Runtime.getRuntime().exec(command); 
                new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
                new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
                PrintWriter stdin = new PrintWriter(p.getOutputStream());
                stdin.println("cd "+download_path);
                switch (listaAc){
                    case(0):{
                        if (size.equals(""))
                            stdin.println("youtube.exe -f \"bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best\" -o %(title)s.mp4 "+pelicula.getUrl());
                        else
                            stdin.println("youtube.exe -f \"bestvideo[height<="+size+"][ext=mp4]+bestaudio[ext=m4a]/best[height<="+size+"][ext=mp4]/best\" -o %(title)s.mp4 "+pelicula.getUrl());
                        break;}
                    case(1):{
                        String rutaList=pelicula.getUrl().split("&list=")[1];
                        if (size.equals(""))
                            stdin.println("youtube.exe -i "+rutaList+" -f \"bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best\" -o %(title)s.mp4");
                        else
                            stdin.println("youtube.exe -i "+ rutaList+" -f \"bestvideo[height<="+size+"][ext=mp4]+bestaudio[ext=m4a]/best[height<="+size+"][ext=mp4]/best\" -o %(title)s.mp4");
                        break;}
                }
                //Para playlist LUEGO DE LIST=
                //stdin.println("youtube-dl -i PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG -f \"bestvideo[height<=720][ext=mp4]+bestaudio[ext=m4a]/best[height<=720]\" -o %(title)s.mp4");
                //NOMRALstdin.println("youtube-dl.exe "+pelicula.getUrl()+ " -f \"bestvideo[height<=720][ext=mp4]+bestaudio[ext=m4a]/best[height<=720]\" -o %(title)s.mp4 ");
                stdin.close();
                p.waitFor();
                JOptionPane.showMessageDialog(null, "Descarga Exitosa "+pelicula.getNombre()+"\n en "+SavePath,"Atención!", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }
}
