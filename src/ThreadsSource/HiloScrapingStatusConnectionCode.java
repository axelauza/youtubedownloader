/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadsSource;

import java.io.IOException;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

/**
 *
 * @author Axel
 */
public class HiloScrapingStatusConnectionCode extends Thread {
    Response[] response;
    String url;
    public HiloScrapingStatusConnectionCode(Response[] response, String url){
        this.response=response;
        this.url=url;
    }
    public void run(){
        try {
            response[0] = Jsoup.connect(url).userAgent("Chrome/41.0.2228.0").timeout(100000).ignoreHttpErrors(true).execute();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el Status Code: " + ex.getMessage());
        }
    
    }
}
