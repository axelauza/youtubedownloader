/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadsSource;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Axel
 */
public class HiloScrapingHtmlDocument extends Thread{
    Document[] doc;
    String url;
    public HiloScrapingHtmlDocument(Document[] doc, String url){
        this.doc=doc;
        this.url=url;
    }
    public void run(){
        
        try {
            doc[0] = Jsoup.connect(url).userAgent("Chrome/41.0.2228.0").timeout(100000).get();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el HTML de la página" + ex.getMessage());
        }
    
    }
    
}
