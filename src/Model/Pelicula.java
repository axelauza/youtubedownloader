/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Controller.*;
import ThreadsSource.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author Axel
 */
public class Pelicula {
    private String nombre;
    private String url;
    private Byte img;
    private String duracion;

    public Pelicula(String nombre, String url) {
        this.nombre = nombre;
        this.url = url;
    }
    public Pelicula(String url) {
        this.url = url;
    }

    public void Descargar(Pelicula pelicula,int listaAc,String size){
        
        HiloDescarga t1=new HiloDescarga(pelicula,listaAc,size);
        t1.start();
        
        
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Byte getImg() {
        return img;
    }

    public void setImg(Byte img) {
        this.img = img;
    }
    public boolean isList(){
        if(url.split("&list=").length==2)
            return true;
        else
            return false;
    }
    @Override
    public String toString(){
        return this.nombre;
    }
}
