/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import ThreadsSource.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Axel
 */
public class Scraping_allPost {
    //public static final String url = "https://jarroba.com/page/%s/";
    public static final int maxPages = 20;

    public static ArrayList<Pelicula> init (String url) {
	ArrayList<Pelicula> peliculas=new ArrayList<Pelicula>();
        String urlPage = String.format(url, "");
        System.out.println("Comprobando entradas de: "+urlPage);

        // Compruebo si me da un 200 al hacer la petición
        if (getStatusConnectionCode(urlPage) == 200) {

            // Obtengo el HTML de la web en un objeto Document2
            Document document = getHtmlDocument(urlPage);
            // Busco todas las historias de meneame que estan dentro de: 
            //Elements entradas = document.select("div.col-md-4.col-xs-12").not("div.col-md-offset-2.col-md-4.col-xs-12");
            Elements entradas = document.select("a");
            /*System.out.println(entradas);*/
            for(Element elemen: entradas){
                if(elemen.attr("class").equals("yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink      spf-link ") && !(elemen.text().isEmpty())){
                    Pelicula pelicula=new Pelicula(elemen.text(),"https://www.youtube.com"+elemen.attr("href"));
                    peliculas.add(pelicula);
                    /*System.out.println(elemen.attr("href"));
                    System.out.println(elemen.text()+"\n");*/
                }
            }

        }else{
            System.out.println("El Status Code no es OK es: "+getStatusConnectionCode(urlPage));
    
        }
    return peliculas;
    }
	
	
    /**
     * Con esta método compruebo el Status code de la respuesta que recibo al hacer la petición
     * EJM:
     * 		200 OK			300 Multiple Choices
     * 		301 Moved Permanently	305 Use Proxy
     * 		400 Bad Request		403 Forbidden
     * 		404 Not Found		500 Internal Server Error
     * 		502 Bad Gateway		503 Service Unavailable
     * @param url
     * @return Status Code
     */
    public static int getStatusConnectionCode(String url) {
	final Response[] response = new Response[1];   
        try {
            
            
            HiloScrapingStatusConnectionCode t1=new HiloScrapingStatusConnectionCode(response,url);
            t1.start();
            t1.join();
            
            
            
        } catch (InterruptedException ex) {
            Logger.getLogger(Scraping_allPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response[0].statusCode();
    }
	
	
    /**
     * Con este método devuelvo un objeto de la clase Document con el contenido del
     * HTML de la web que me permitirá parsearlo con los métodos de la librelia JSoup
     * @param url
     * @return Documento con el HTML
     */
    public static Document getHtmlDocument(String url) {

        final Document[] doc = new Document[1];
        try{
            HiloScrapingHtmlDocument t1=new HiloScrapingHtmlDocument(doc,url);
            t1.start();
            t1.join();
        }catch(InterruptedException ex) {
            Logger.getLogger(Scraping_allPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doc[0];

    }
}
